package com.example.firstopengl;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.os.SystemClock;
import android.util.Log;

public class MyGLRenderer implements GLSurfaceView.Renderer {

	private static final String TAG = "MyGLRenderer";
	private Triangle mTriangle;
	private float[] mProjectionMatrix = new float[16];
	private float[] mViewMatrix =  new float[16];
	private float[] mMVPMatrix = new float[16];
	private float[] mRotationMatrix = new float[16];
	
	public void onDrawFrame(GL10 unused) {
		// Redraw background color
	    GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
	    
	    // set the camera position (view matrix)
	    Matrix.setLookAtM(mViewMatrix, 0, 0, 0, -3, 0f, 0f, 0f, 0f, 1.0f, 0.0f);
	    
	    // Calculate the projection and view transformation
	    Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mViewMatrix, 0);
	    
	    float[] scratch = new float[16];
	    // Create a rotation transformation for the triangle
	    long time = SystemClock.uptimeMillis() % 4000L;
	    float angle = 0.090f * ((int) time);
	    Matrix.setRotateM(mRotationMatrix, 0, angle, 0, 0, -1.0f);
	    
	    // Combine the rotation matrix with the projection and camera view
	    // Note that the mMVPMatrix factor *must be first* in order
	    // for the matrix multiplication product to be correct.
	    Matrix.multiplyMM(scratch, 0, mMVPMatrix, 0, mRotationMatrix, 0);
	    
	    // draw the shape
	    mTriangle.draw(scratch);
	}

	public void onSurfaceChanged(GL10 unused, int width, int height) {
		GLES20.glViewport(0, 0, width, height);
		
		float ratio = (float) width / height;
		
		// this projection matrix is applied to object coordinates
		// in the onDrawFrame() method
		Matrix.frustumM(mProjectionMatrix, 0, -ratio, ratio, -1, 1, 3, 7);
	}

	public void onSurfaceCreated(GL10 unused, EGLConfig config) {
		// Set the background frame color
	    GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	    // initialize a triangle
	    mTriangle = new Triangle();
	}
	
	public static int loadShader(int type, String shaderCode){
		// create a vertex shader type (GLES20.GL_VERTEX_SHADER)
		// or a fragment shader type (GLES20.GL_FRAGMENT_SHADER)
		int shader = GLES20.glCreateShader(type);
		
		// add the source code to the shader and compile it
		GLES20.glShaderSource(shader, shaderCode);
		GLES20.glCompileShader(shader);
		
		return shader;
	}
	
	/**
	    * Utility method for debugging OpenGL calls. Provide the name of the call
	    * just after making it:
	    *
	    * <pre>
	    * mColorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");
	    * MyGLRenderer.checkGlError("glGetUniformLocation");</pre>
	    *
	    * If the operation is not successful, the check throws an error.
	    *
	    * @param glOperation - Name of the OpenGL call to check.
	    */
	    public static void checkGlError(String glOperation) {
	        int error;
	        while ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
	            Log.e(TAG, glOperation + ": glError " + error);
	            throw new RuntimeException(glOperation + ": glError " + error);
	        }
	    }

	
}
